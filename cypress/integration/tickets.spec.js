describe('Tickets', () => {
    beforeEach(() => cy.visit('https://bit.ly/2XSuwCW'))
    
    it('Fill all the text input fields', () => {
        const firstName = 'Davi'
        const lastName = 'Vitorino'

        cy.get('#first-name').type(firstName)
        cy.get('#last-name').type(lastName)
        cy.get('#email').type('davitorino26@gmail.com')
        cy.get('#requests').type('Cool text')
        cy.get('#signature').type(`${firstName} ${lastName}`)
    })
    
    it('Select two tickets', () => {
        cy.get('#ticket-quantity').select('2')
    })

    it('Select vip ticket type', () => {
        cy.get('#vip').check()
    })

    it('Select checkboxes', () => {
        cy.get('#social-media').check();
        cy.get('#friend').check()
    })

    it('Select friend and publication, then uncheck friend', () => {
        cy.get('#friend').check()
        cy.get('#publication').check()
        cy.get('#friend').uncheck()
    })

    it('Title validation', () => {
        cy.get('header h1').should('contain', 'TICKETBOX')
        
    })

    it('Alerts on invalid email', () => {
        cy.get('#email')
          .as('email')
          .type('davitorino26gmail.com')

        cy.get('#email.invalid').should('exist')

        cy.get('@email')
          .clear()
          .type('davitorino26@gmail.com')

        cy.get('#email.invalid').should('not.exist')
    })

    it('Fills the form and reset it', () => {
        const firstName = 'Davi'
        const lastName = 'Vitorino'
        const fullName = `${firstName} ${lastName}`

        cy.get('#first-name').type(firstName)
        cy.get('#last-name').type(lastName)
        cy.get('#email').type('davitorino26@gmail.com')
        cy.get('#ticket-quantity').select('2')
        cy.get('#vip').check()
        cy.get('#friend').check()
        cy.get('#requests').type('I like cakes')
        
        cy.get('.agreement p').should(
            'contain',
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        )

        cy.get('#agree').click()
        cy.get('#signature').type(fullName)

        cy.get('button[type="submit"]')
          .as('submitButton')
          .should('not.be.disabled')

        cy.get('button[type="reset"]').click()

        cy.get('@submitButton').should('be.disabled')
    })

    it('Fills mandatory fields using support command', () => {
        const customer = {
            firstName: 'João',
            lastName:'Silva',
            email: 'joaosilva@example.com'
        }

        cy.fillMandatoryFields(customer)
        
        cy.get('button[type="submit"]')
          .as('submitButton')
          .should('not.be.disabled')

        cy.get('#agree').uncheck()

        cy.get('@submitButton').should('be.disabled')
    })
})